"use strict";
const { isSupported, createLocalVideoTrack } = require("twilio-video");

const { isMobile } = require("./browser");
const { connect } = require("./socket");

const { joinRoom, mute, unmute, showVideo, hideVideo } = require("./joinroom");
const micLevel = require("./miclevel");
const selectMedia = require("./selectmedia");
const selectRoom = require("./selectroom");
const showError = require("./showerror");
const { setPage, axios } = require("./utils");
const roomController = require("./room");
const { pages, url } = require("./constants");
const { addRoomClicked, submitRoomForm } = require("./createRoom");

const $modals = $("#modals");
const $selectMicModal = $("#select-mic", $modals);
const $selectCameraModal = $("#select-camera", $modals);
const $showErrorModal = $("#show-error", $modals);
const $joinRoomModal = $("#join-room", $modals);

connect();
// ConnectOptions settings for a video web application.
const connectOptions = {
	// Available only in Small Group or Group Rooms only. Please set "Room Type"
	// to "Group" or "Small Group" in your Twilio Console:
	// https://www.twilio.com/console/video/configure
	bandwidthProfile: {
		video: {
			dominantSpeakerPriority: "high",
			mode: "collaboration",
			clientTrackSwitchOffControl: "auto",
			contentPreferencesMode: "auto",
		},
	},

	// Available only in Small Group or Group Rooms only. Please set "Room Type"
	// to "Group" or "Small Group" in your Twilio Console:
	// https://www.twilio.com/console/video/configure
	dominantSpeaker: true,

	// Comment this line if you are playing music.
	maxAudioBitrate: 16000,

	// VP8 simulcast enables the media server in a Small Group or Group Room
	// to adapt your encoded video quality for each RemoteParticipant based on
	// their individual bandwidth constraints. This has no utility if you are
	// using Peer-to-Peer Rooms, so you can comment this line.
	preferredVideoCodecs: [{ codec: "VP8", simulcast: true }],

	// Capture 720p video @ 24 fps.
	video: { height: 720, frameRate: 24, width: 1280 },
};

// For mobile browsers, limit the maximum incoming video bitrate to 2.5 Mbps.
if (isMobile) {
	connectOptions.bandwidthProfile.video.maxSubscriptionBitrate = 2500000;
}

// On mobile browsers, there is the possibility of not getting any media even
// after the user has given permission, most likely due to some other app reserving
// the media device. So, we make sure users always test their media devices before
// joining the Room. For more best practices, please refer to the following guide:
// https://www.twilio.com/docs/video/build-js-video-application-recommendations-and-best-practices
const deviceIds = {
	audio: isMobile ? null : localStorage.getItem("audioDeviceId"),
	video: isMobile ? null : localStorage.getItem("videoDeviceId"),
};

const getAppId = () => {
	return new Promise((resolve, reject) => {
		buildfire.getContext((err, context) => {
			if (err) {
				reject(err);
			}
			resolve(context.appId);
		});
	});
};

/**
 * Select your Room name, your screen name and join.
 * @param [error=null] - Error from the previous Room session, if any
 */
async function joinRoomUsingName() {
	try {
		const appId = await getAppId();
		const room = window.currentRoom;
		const roomJoin = room.roomjoineds.find((i) => i.userId == loggedInUser.userId);
		if (roomJoin && roomJoin.status === "banned") {
			$("#banned-user").modal("show");
			return;
		}
		setPage(pages.VIEW_ROOM);
		const roomName = room.name.replace(" ", "-").trim();
		const identity = loggedInUser.username.split("@")[0];
		await axios.post(`${url}/v1/room-joined`, {
			userId: loggedInUser._id,
			roomId: room._id,
		});
		const response1 = await axios.get(`${url}/v1/auth/get-all-rooms/${appId}`);
		const rooms = response1.room;
		const updatedRoom = rooms.find((r) => r._id === room._id);
		roomController.setRoom(updatedRoom);
		// Fetch an AccessToken to join the Room.
		const response = await fetch(
			`${url}/v1/twilio/token?identity=${identity}&roomName=${roomName}`
		);

		// Extract the AccessToken from the Response.
		const token = await response.text();

		// Add the specified audio device ID to ConnectOptions.
		connectOptions.audio = { deviceId: { exact: deviceIds.audio } };

		// Add the specified Room name to ConnectOptions.
		connectOptions.name = roomName;

		// Add the specified video device ID to ConnectOptions.
		connectOptions.video.deviceId = { exact: deviceIds.video };
		connectOptions.automaticSubscription = false;

		// Join the Room.
		await joinRoom(token, connectOptions);
		// After the video session, display the room selection modal.
	} catch (error) {
		// return selectAndJoinRoom(error);
	}
}
/**
 * Select your camera.
 */
async function selectCamera() {
	if (deviceIds.video === null) {
		try {
			deviceIds.video = await selectMedia("video", $selectCameraModal, (videoTrack) => {
				// const $video = $("video", $selectCameraModal);
				// videoTrack.attach($video.get(0));
			});
		} catch (error) {
			showError($showErrorModal, error);
			return;
		}
	}
}

function htmlDecode(input) {
	const doc = new DOMParser().parseFromString(input, "text/html");
	return doc.documentElement.textContent;
}

async function getContent() {
	const appId = await getAppId();
	axios.get(`${url}/v1/admin/content/${appId}`).then(async (data) => {
		const htmlString = data?.content?.contentText ?? null;
		if (htmlString) {
			const html = htmlDecode(htmlString);
			document.getElementById("textFromApp").innerHTML = html;
		}
	});

	axios.get(`${url}/v1/admin/carousel/${appId}`).then((response) => {
		const images = response.images;
		if (images.length >= 0) {
			document.getElementById("appImages").innerHTML = null;
		}
		images.forEach((item, index) => {
			const div = document.createElement("div");
			div.classList.add("carousel-item");
			if (index === 0) {
				div.classList.add("active");
			}
			const img = document.createElement("img");
			img.className = "d-block w-100 sliderImage";
			img.setAttribute("src", item.imageUrl);
			div.append(img);
			document.getElementById("appImages").append(div);
		});
	});
}

/**
 * Select your microphone.
 */
async function selectMicrophone() {
	setPage(pages.HOME);
	$(".unmute-mic-btn").hide();
	$(".mute-mic-btn").show();
	$(".show-video-btn").show();
	$(".hide-video-btn").hide();
	getContent();
	buildfire.messaging.onReceivedMessage = function (message) {
		switch (message.section) {
			case "updateContent":
				getContent();
				break;
		}
		switch (message.section) {
			case "updateRooms":
				roomController.getRooms();
				break;
		}
	};
	if (deviceIds.audio === null) {
		try {
			deviceIds.audio = await selectMedia("audio", $selectMicModal, (audioTrack) => {
				const $levelIndicator = $("svg rect", $selectMicModal);
				const maxLevel = Number($levelIndicator.attr("height"));
				micLevel(audioTrack, maxLevel, (level) =>
					$levelIndicator.attr("y", maxLevel - level)
				);
			});
		} catch (error) {
			showError($showErrorModal, error);
			return;
		}
	}
	return selectCamera();
}

roomController.getRoomTopics();
roomController.getRooms();
window.getRooms = roomController.getRooms;

const goToHome = () => setPage(pages.HOME);

function addRoomClickHandle() {
	$("nav .navbar-toggler").addClass("collapsed");
	$("nav .navbar-collapse").removeClass("show");
	addRoomClicked();
}

async function leaveRoom() {
	await roomController.removeUserFromRoom(window.loggedInUser._id, window.currentRoom._id);
	window.room.disconnect();
	roomController.getRooms();
	setPage(pages.HOME);
}

function endRoom() {
	window.currentRoom.roomjoineds.forEach((joined, index, arr) => {
		roomController.removeUserFromRoom(joined.userId, window.currentRoom._id);
		if (index === arr.length - 1) {
			roomController.getRooms();
		}
	});
	setPage(pages.HOME);
}

async function muteClicked() {
	const room = window.currentRoom;
	const payload = {
		roomId: room._id,
		userId: loggedInUser.userId,
	};
	await axios.post(`${url}/v1/room/mute-user`, payload);
	await axios.post(`${url}/v1/twilio/update-rules`, {
		roomName: room.name,
	});
	socketConnection.emit("refresh-room", room._id);
	mute();
}

async function unmuteClicked() {
	const room = window.currentRoom;
	const payload = {
		roomId: room._id,
		userId: loggedInUser.userId,
	};
	await axios.post(`${url}/v1/room/unmute-user`, payload);
	await axios.post(`${url}/v1/twilio/update-rules`, {
		roomName: room.name,
	});
	socketConnection.emit("refresh-room", room._id);
	unmute();
}

async function showVideoClicked() {
	const room = window.currentRoom;
	const payload = {
		roomId: room._id,
		userId: loggedInUser.userId,
		show: false,
	};
	await axios.post(`${url}/v1/room/camera`, payload);
	await axios.post(`${url}/v1/twilio/update-rules`, {
		roomName: room.name,
	});
	socketConnection.emit("refresh-room", room._id);
	showVideo();
}

async function hideVideoClicked() {
	const room = window.currentRoom;
	const payload = {
		roomId: room._id,
		userId: loggedInUser.userId,
		show: true,
	};
	await axios.post(`${url}/v1/room/camera`, payload);
	await axios.post(`${url}/v1/twilio/update-rules`, {
		roomName: room.name,
	});
	socketConnection.emit("refresh-room", room._id);
	hideVideo();
}

function searchRooms(text) {
	roomController.getRooms();
	$("#roomInput").val(text);
	roomController.getRooms("room-list-search", text);
	setPage(pages.ROOM_LIST);
}

function inputChanged(e) {
	if (e.key === "Enter") {
		const text = e.target.value;
		searchRooms(text);
	}
}

function searchClicked() {
	const text = $("#searchRoomInput").val();
	searchRooms(text);
}

function loginClicked() {
	buildfire.auth.login();
}

$("#createRoomBtn").click(addRoomClickHandle);
$("#cancelRoom").click(goToHome);
$("#logo").click(goToHome);
$("#createRoomForm").submit(submitRoomForm);
$("#join-room-btn").click(joinRoomUsingName);
$("#leave-room-btn").click(leaveRoom);
$("#end-room-btn").click(endRoom);
$(".mute-mic-btn.clickable").click(muteClicked);
$(".unmute-mic-btn.clickable").click(unmuteClicked);
$(".show-video-btn.clickable").click(showVideoClicked);
$(".hide-video-btn.clickable").click(hideVideoClicked);
$("#searchRoomInput").keydown(inputChanged);
$("#searchRoomInputButton").click(searchClicked);
$("#roomInput").keydown(inputChanged);
$("#roomInputButton").click(searchClicked);
$("#loginBtn").click(loginClicked);

const currentPage = localStorage.getItem("currentPage");

const registerUser = (user) => {
	if (user) {
		user.name = user.email.split("@")[0];
		window.loggedInUser = user;
		const data = {
			email: user.email,
			password: "SydneyUser123",
			name: user.email.split("@")[0],
			_id: user._id,
		};
		axios.post(`${url}/v1/auth/user`, data);
	}
};

buildfire.auth.getCurrentUser((err, user) => {
	registerUser(user);
	roomController.getRooms();
});

buildfire.auth.onLogin((user) => {
	registerUser(user);
	roomController.getRooms();
});

buildfire.auth.onLogout(() => {
	roomController.getRooms();
});

// If the current browser is not supported by twilio-video.js, show an error
// message. Otherwise, start the application.
window.addEventListener(
	"load",
	isSupported
		? selectMicrophone
		: () => {
				showError($showErrorModal, new Error("This browser is not supported."));
		  }
);
