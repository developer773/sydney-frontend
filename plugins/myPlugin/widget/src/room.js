const { url, pages } = require("./constants");
const { updateRules } = require("./joinroom");
const { axios, setPage } = require("./utils");
const { getAppId } = require("./createRoom");

function isUserLoggedIn() {
	return new Promise((resolve, reject) => {
		buildfire.auth.getCurrentUser((err, user) => {
			if (err) reject(err);
			resolve(user);
		});
	});
}

const openJoinRoomDialog = (room) => async () => {
	const isLoggedIn = await isUserLoggedIn();
	if (!isLoggedIn) {
		buildfire.auth.login();
		return;
	}
	setRoom(room);
	$("#room-details").modal({
		focus: true,
		show: true,
	});
};

const makeHost = (user) => async () => {
	try {
		const room = window.currentRoom;
		const payload = {
			roomId: room._id,
			userId: user.user.id,
		};
		await axios.post(`${url}/v1/room/make-host`, payload);
		fetchRoom();
		await axios.post(`${url}/v1/twilio/update-rules`, {
			roomName: room.name,
		});
		socketConnection.emit("refresh-room", room._id);

		$("#participant-options").modal("hide");

		e.stopPropagation();
	} catch (e) {
		console.log(e);
	}
};

const removeHost = (user) => async (e) => {
	try {
		const room = window.currentRoom;
		const payload = {
			roomId: room._id,
			userId: user.user.id,
		};
		await axios.post(`${url}/v1/room/remove-host`, payload);
		fetchRoom();
		await axios.post(`${url}/v1/twilio/update-rules`, {
			roomName: room.name,
		});
		socketConnection.emit("refresh-room", room._id);

		$("#participant-options").modal("hide");
	} catch (e) {
		console.log(e);
	}
};

const removeSpeaker = (user) => async (e) => {
	const room = window.currentRoom;
	const payload = {
		roomId: room._id,
		userId: user.user.id,
		status: "inactive",
	};
	await axios.post(`${url}/v1/room/speaker`, payload);
	fetchRoom();
	await axios.post(`${url}/v1/twilio/update-rules`, {
		roomName: room.name,
	});
	socketConnection.emit("refresh-room", room._id);

	e.stopPropagation();
};

const addSpeaker = (user) => async (e) => {
	const room = window.currentRoom;
	const payload = {
		roomId: room._id,
		userId: user.user.id,
		status: "active",
	};
	await axios.post(`${url}/v1/room/speaker`, payload);
	fetchRoom();
	await axios.post(`${url}/v1/twilio/update-rules`, {
		roomName: room.name,
	});
	socketConnection.emit("refresh-room", room._id);

	e.stopPropagation();
};

const muteMicrophone = (user) => async (e) => {
	const room = window.currentRoom;
	const payload = {
		roomId: room._id,
		userId: user.user.id,
	};
	await axios.post(`${url}/v1/room/mute-user`, payload);
	fetchRoom();
	await axios.post(`${url}/v1/twilio/update-rules`, {
		roomName: room.name,
	});
	socketConnection.emit("refresh-room", room._id);
	$("#participant-options").modal("hide");
};

const unmuteMicrophone = (user) => async (e) => {
	const room = window.currentRoom;
	const payload = {
		roomId: room._id,
		userId: user.user.id,
	};
	await axios.post(`${url}/v1/room/unmute-user`, payload);
	fetchRoom();
	await axios.post(`${url}/v1/twilio/update-rules`, {
		roomName: room.name,
	});
	socketConnection.emit("refresh-room", room._id);

	$("#participant-options").modal("hide");
};

const blockUser = (user) => async (e) => {
	const room = window.currentRoom;
	const payload = {
		roomId: room._id,
		userId: user.user.id,
	};
	await axios.post(`${url}/v1/room/block-user`, payload);
	fetchRoom();
	await axios.post(`${url}/v1/twilio/update-rules`, {
		roomName: room.name,
	});
	socketConnection.emit("refresh-room", room._id);

	$("#participant-options").modal("hide");
};

const createMicRequest = async (e) => {
	const room = window.currentRoom;
	const payload = {
		roomId: room._id,
		userId: loggedInUser.userId,
	};
	await axios.post(`${url}/v1/request/create`, payload);
	$("#participant-options").modal("hide");
	fetchRoom();
	socketConnection.emit("refresh-room", room._id);
};

const createVideoRequest = async (e) => {
	const room = window.currentRoom;
	const payload = {
		roomId: room._id,
		userId: loggedInUser.userId,
		type: "video",
	};
	await axios.post(`${url}/v1/request/create`, payload);
	$("#participant-options").modal("hide");
	fetchRoom();
	socketConnection.emit("refresh-room", room._id);
};

const acceptRequest = (joinedId, type) => async (e) => {
	const room = window.currentRoom;
	const payload = {
		joinedId,
		type,
	};
	await axios.post(`${url}/v1/request/accept`, payload);
	fetchRoom();
	await axios.post(`${url}/v1/twilio/update-rules`, {
		roomName: room.name,
	});
	socketConnection.emit("refresh-room", room._id);
};

const rejectRequest = (joinedId, type) => async (e) => {
	const room = window.currentRoom;
	const payload = {
		joinedId,
		type,
	};
	await axios.post(`${url}/v1/request/reject`, payload);
	fetchRoom();
	socketConnection.emit("refresh-room", room._id);
};

const participantClicked = (user, userType) => () => {
	$("#speakerOption").off("click");
	$("#muteMicrophone").off("click");
	$("#make-host-btn").off("click");
	$("#block-btn").off("click");
	if (userType === "speakers") {
		$("#speakerOption").click(removeSpeaker(user));
		$("#speakerOption span").text("Remove from speakers");
	} else {
		$("#speakerOption").click(addSpeaker(user));
		$("#speakerOption span").text("Add to speakers");
	}
	const hosts = currentRoom?.hosts?.map((i) => i.id);
	const isHost = hosts.includes(loggedInUser.userId);
	if (!isHost) {
		return;
	}
	if (hosts?.includes(user.userId)) {
		$("#make-host-btn span").text("Remove from host");
		$("#make-host-btn").click(removeHost(user));
	} else {
		$("#make-host-btn span").text("Make host");
		$("#make-host-btn").click(makeHost(user));
	}
	$("#muteMicrophone span").text("Mute their mic");
	$("#muteMicrophone").on("click", muteMicrophone(user));
	$("#block-btn").on("click", blockUser(user));
	$("#participant-options-name").text(user.user.name);
	$("#participant-options").modal({
		backdrop: "static",
		focus: true,
		keyboard: false,
		show: true,
	});
};

const removeUserFromRoom = async (userId, roomId) => {
	const data = {
		roomId,
		userId,
	};
	await axios.delete(`${url}/v1/room-joined`, data);
};

const deleteRoom = (room) => () => {
	console.log(room.userId, loggedInUser._id);
	if (room.userId !== loggedInUser._id) {
		$("#banned-delete").modal("show");
	} else {
		axios.delete(`${url}/v1/auth/get-all-rooms/${room._id}`).then((response) => {
			buildfire.messaging.sendMessageToControl({ section: "updateRooms" });
			getRooms();
		});
	}
};

async function getRooms(id = "room-list", searchText = "") {
	buildfire.auth.getCurrentUser(async (err, user) => {
		if (err || !user) {
			$('#guest-user').show();
			$('#room-list').html(null);
		} else {
			$('#guest-user').hide();
			const appId = await getAppId();
			const response = await axios.get(`${url}/v1/auth/get-all-rooms/${appId}`);
			let rooms = response.room;
			const roomsElement = document.getElementById(id);
			roomsElement.innerHTML = null;
			if (searchText) {
				rooms = rooms.filter((i) =>
					i.name.toLowerCase().match(new RegExp(searchText.toLowerCase()))
				);
			}
			rooms
				.filter((room) => !room.isPrivate)
				.forEach((room) => {
					let blueprint = document.getElementById("room-blueprint");
					blueprint = blueprint.cloneNode(true);
					blueprint.querySelector(".room-title").innerHTML = room.name;
					blueprint.querySelector(".room-topic").innerHTML = room.topic;
					blueprint.querySelector(".room-description").innerHTML = room.description;
					blueprint
						.querySelector(".delete-room")
						.addEventListener("click", deleteRoom(room));
					const notBannedRooms = room.roomjoineds?.filter(
						(join) => join.status !== "banned"
					);
					blueprint.querySelector(".participants").innerHTML = notBannedRooms?.length;
					blueprint
						.querySelector(".join-btn")
						.addEventListener("click", openJoinRoomDialog(room));
					roomsElement.append(blueprint);
				});
		}
	});
}

const appendUsers = (id, room) => (item) => {
	console.log(item);
	const blueprint = document.getElementById("participant-blueprint").cloneNode(true);
	blueprint.querySelector("p").innerHTML = item.user.name;
	$("#participant-list").append(blueprint);

	const div = document.createElement("div");
	div.classList.add("user", "d-flex", "flex-column", "cursor-pointer");

	const img = document.createElement("img");
	img.setAttribute(
		"src",
		"https://images.unsplash.com/photo-1633332755192-727a05c4013d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80"
	);

	const pName = document.createElement("p");
	pName.classList.add("text-center", "mb-0");
	pName.innerText = item.user.name;
	div.appendChild(img);
	div.appendChild(pName);
	const hosts = room?.hosts?.map((i) => i.id);
	if (hosts?.includes(item.user.id)) {
		const hostName = document.createElement("p");
		hostName.classList.add("text-center", "text-primary");
		hostName.innerText = "Host";
		div.appendChild(hostName);
	}
	const audienceNode = div.cloneNode(true);
	audienceNode.addEventListener("click", participantClicked(item, id));
	$(`#${id}`).append(audienceNode);
};

function hideMuteCameraOptions() {
	$(".mic-camera-options").hide();
}

function showMuteCameraOptions() {
	$(".mic-camera-options").show();
	$(".request-mic-camera-options").hide();
	$(".mic-options").hide();
	$(".video-options").hide();
}

function setRoom(room) {
	window.currentRoom = room;
	$("#mic-request .accept-btn").off("click");
	$("#mic-request .reject-btn").off("click");
	$(".request-mic").off("click");
	$(".request-mic").click(createMicRequest);
	$(".request-video").off("click");
	$(".request-video").click(createVideoRequest);

	const hosts = room?.hosts?.map((i) => i.id);
	const isHost = hosts.includes(loggedInUser.userId);
	if (isHost) {
		$("div #end-room-btn").show();
		$(".end-room").show();

		showMuteCameraOptions();

		$(".request-mic").hide();
		$(".request-video").hide();
		const joined = room.roomjoineds.find(
			(i) => i.micRequestStatus === "requested" || i.videoRequestStatus === "requested"
		);
		if (joined) {
			$("#mic-request").show();
			if (joined.micRequestStatus === "requested") {
				$("#mic-request p").text(
					`${joined.user.name} has requested to speak. Allow them to speak?`
				);
				$("#mic-request .accept-btn").off("click");
				$("#mic-request .reject-btn").off("click");
				$("#mic-request .accept-btn").click(acceptRequest(joined._id));
				$("#mic-request .reject-btn").click(rejectRequest(joined._id));
			}
			if (joined.videoRequestStatus === "requested") {
				$("#mic-request p").text(
					`${joined.user.name} has requested camera. Do you want to allow?`
				);
				$("#mic-request .accept-btn").off("click");
				$("#mic-request .reject-btn").off("click");
				$("#mic-request .accept-btn").click(acceptRequest(joined._id, "camera"));
				$("#mic-request .reject-btn").click(rejectRequest(joined._id, "camera"));
			}
		} else {
			$("#mic-request").hide();
		}
	} else {
		$("div #end-room-btn").hide();
		$(".end-room").hide();
		hideMuteCameraOptions();
		$("#mic-request").hide();

		const roomJoin = room.roomjoineds.find((i) => i.userId == loggedInUser.userId);
		if (roomJoin) {
			if (roomJoin.status === "banned") {
				getRooms();
				setPage(pages.HOME);
				return;
			}
			if (
				roomJoin.micRequestStatus !== "accepted" ||
				roomJoin.videoRequestStatus !== "accepted"
			) {
				$(".request-mic-camera-options").show();
			}
			if (roomJoin.micRequestStatus === "accepted") {
				$(".mic-options").show();
				$(".request-mic").hide();
			} else if (roomJoin.micRequestStatus === "requested") {
				$(".mic-options").hide();
				$(".request-mic button").text("Mic Requested");
				$(".request-mic").off("click");
			} else {
				$(".request-mic button").text("Request Mic");
				$(".mic-options").hide();
				$(".request-mic").show();
			}
			if (roomJoin.videoRequestStatus === "accepted") {
				$(".video-options").show();
				$(".request-video").hide();
			} else if (roomJoin.videoRequestStatus === "requested") {
				$(".video-options").hide();
				$(".request-video button").text("Video Requested");
				$(".request-video").off("click");
			} else {
				$(".request-video button").text("Request Video");
				$(".video-options").hide();
				$(".request-video").show();
			}
		} else {
			$(".mic-options").hide();
			$(".video-options").hide();
			$(".request-mic").show();
			$(".request-video").show();
		}
	}
	$(".room-title").text(room.name);
	$(".room-topic").text(room.topic);
	$(".room-description").text(room.description);
	const notBannedRooms = room.roomjoineds?.filter((join) => join.status !== "banned");
	$(".participant-count").text(notBannedRooms.length);
	$("#participant-list").html("");
	$(`#speakers`).html("");
	$(`#audiences`).html("");
	const notBannedUsers = room.roomjoineds?.filter((room) => room.status !== "banned");
	const speakers = notBannedUsers.filter((i) => i.isSpeaker);
	const audiences = notBannedUsers.filter((i) => !i.isSpeaker);
	speakers.forEach(appendUsers("speakers", room));
	audiences.forEach(appendUsers("audiences", room));
	$(".host-name").text(loggedInUser.name);
}

async function getRoomTopics() {
	// const response = await axios.get(`${url}/v1/auth/room-topic`);
	// const roomTopics = response.roomTopics;
}

async function fetchRoom() {
	const roomJoin = window.currentRoom.roomjoineds.find((i) => i.userId == loggedInUser.userId);
	if (!roomJoin || roomJoin.status === "banned") {
		window.room.disconnect();
		setPage(pages.HOME);
	}
	const currentRoom = window.currentRoom;
	const roomResponse = await axios.get(`${url}/v1/auth/rooms/${currentRoom._id}`);
	setRoom(roomResponse);
}

module.exports = {
	getRooms,
	getRoomTopics,
	setRoom,
	removeUserFromRoom,
	fetchRoom,
};
