const { url } = require("./constants");

var socket;
function connect() {
	socket = io(url);
	window.socketConnection = socket;
}

module.exports = {
	socket,
	connect,
};
