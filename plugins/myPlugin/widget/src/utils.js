const { pages } = require("./constants");

function isUserLoggedIn() {
	return new Promise((resolve, reject) => {
		buildfire.auth.getCurrentUser((err, user) => {
			if (err) reject(err);
			resolve(user);
		});
	});
}

async function setPage(page) {
	if (page === pages.CREATE_ROOM || page === pages.VIEW_ROOM) {
		const isLoggedIn = await isUserLoggedIn();
		if (!isLoggedIn) {
			buildfire.auth.login();
			return;
		}
	}
	if (page === pages.HOME) {
		getRooms();
	}

	if (page === pages.VIEW_ROOM) {
		socketConnection.emit("connect-user",{
			user: loggedInUser._id,
			room: window.currentRoom._id
		});
	}
	Object.values(pages).forEach((id) => {
		$(`#${id}`).hide();
	});
	$(`#${page}`).show();
}

const axios = {
	get: (link) => {
		return new Promise((resolve, reject) => {
			fetch(link)
				.then((response) => {
					response.json().then((res) => {
						resolve(res);
					});
				})
				.catch((e) => {
					reject(e);
				});
		});
	},
	post: (link, data) => {
		return new Promise((resolve, reject) => {
			fetch(link, {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify(data),
			})
				.then((response) => {
					response.json().then((res) => {
						resolve(res);
					});
				})
				.catch((e) => {
					reject(e);
				});
		});
	},
	delete: (link, data) => {
		return new Promise((resolve, reject) => {
			fetch(link, {
				method: "DELETE",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify(data),
			})
				.then((response) => {
					response.json().then((res) => {
						resolve(res);
					});
				})
				.catch((e) => {
					reject(e);
				});
		});
	},
};

module.exports = {
	setPage,
	axios,
};
