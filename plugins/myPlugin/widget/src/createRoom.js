const { pages, url } = require("./constants");
const { getRooms } = require("./room");
const { setPage, axios } = require("./utils");

async function addRoomClicked() {
	setPage(pages.CREATE_ROOM);
}

const getAppId = () => {
	return new Promise((resolve, reject) => {
		buildfire.getContext((err, context) => {
			if (err) {
				reject(err);
			}
			resolve(context.appId);
		});
	});
};

async function submitRoomForm(event) {
	event.preventDefault();
	const data = new FormData(event.target);
	const entries = Object.fromEntries(data.entries());
	const trimName = entries.title.split(" ").join("-");
	const payload = {
		name: entries.title,
		trimName: trimName,
		description: entries.description,
		topic: entries.topic,
		isPrivate: !!entries.privateRoom,
		userId: loggedInUser._id,
		appId: await getAppId(),
	};
	await axios.post(`${url}/v1/auth/create-room`, payload);
	buildfire.messaging.sendMessageToControl({ section: "updateRooms" });
	setPage(pages.HOME);
}

module.exports = {
	addRoomClicked,
	submitRoomForm,
	getAppId,
};
