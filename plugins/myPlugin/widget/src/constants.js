const mode = "dev";
const LOCAL_API_URL = "http://localhost:3000";
const LIVE_API_URL = "https://sydney.vipankumar.in";
const API_URL = mode === 'dev' ? LOCAL_API_URL : LIVE_API_URL;

const PAGES = {
	HOME: "home-page",
	CREATE_ROOM: "create-room-page",
	VIEW_ROOM: "view-room-page",
	ROOM_LIST: "room-list-page"
};
const current_page = PAGES.HOME;

module.exports = {
	url: API_URL,
	pages: PAGES,
	current_page,
};
