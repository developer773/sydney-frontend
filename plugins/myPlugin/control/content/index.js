// Editor configuration.
ClassicEditor.defaultConfig = {
	toolbar: {
		items: [
			"heading",
			"|",
			"alignment", // <--- ADDED
			"bold",
			"italic",
			"link",
			"bulletedList",
			"numberedList",
			"blockQuote",
			"undo",
			"redo",
		],
	},
	// This value must be kept in sync with the language defined in webpack.config.js.
	language: "en",
};

var editor = ClassicEditor.create(document.querySelector("#editor")).catch((error) => {
	console.error(error);
});

var currentFile = null;
const mode = "dev";
const LOCAL_API_URL = "http://localhost:3000";
const LIVE_API_URL = "https://sydney.vipankumar.in";
var url = mode === "dev" ? LOCAL_API_URL : LIVE_API_URL;
var images = [];
var rooms = [];
var visibleRooms = [];

buildfire.getContext((err, context) => {
	const appId = context.appId;
	window.appId = appId;
	getCarousal(appId);
	getContent(appId);
	getRooms(appId);
});

buildfire.messaging.onReceivedMessage = function (message) {
	switch (message.section) {
		case "updateRooms":
			getRooms(appId);
			break;
	}
};

const removeImage = (image) => () => {
	axios.delete(`${url}/v1/admin/carousel/${image._id}`).then((response) => {
		getCarousal(appId);
		buildfire.messaging.sendMessageToWidget({ section: "updateContent" });
	});
};

const removeRoom = (room) => () => {
	axios.delete(`${url}/v1/auth/get-all-rooms/${room._id}`).then((response) => {
		getRooms(appId);
		buildfire.messaging.sendMessageToWidget({ section: "updateRooms" });
	});
};

function htmlDecode(input) {
	const doc = new DOMParser().parseFromString(input, "text/html");
	return doc.documentElement.textContent;
}

function getContent(appId) {
	axios.get(`${url}/v1/admin/content/${appId}`).then(async (response) => {
		const edit = await editor;
		const { data } = response;
		const htmlString = data.content.contentText;
		edit.data.set(htmlDecode(htmlString));
	});
}

function getRooms(appId) {
	axios.get(`${url}/v1/auth/get-all-rooms/${appId}`).then(({ data }) => {
		rooms = data.room;
		visibleRooms = data.room;
		previewRooms();
	});
}

function previewRooms() {
	document.getElementById("roomList").innerHTML = null;
	visibleRooms.forEach((room) => {
		const div = document.createElement("div");
		div.className = "d-flex align-items-center my-3";

		const p = document.createElement("p");
		p.className = "mb-0";
		p.innerText = room.name;

		const btn = document.createElement("button");
		btn.className = "btn btn-danger ml-3";
		btn.innerText = "Remove";
		btn.addEventListener("click", removeRoom(room));

		div.append(p);
		div.append(btn);

		document.getElementById("roomList").append(div);
	});
}

function getCarousal(appId) {
	const imagesDiv = document.getElementById("images");
	imagesDiv.innerHTML = null;
	axios.get(`${url}/v1/admin/carousel/${appId}`).then((response) => {
		const { data } = response;
		images = data.images;
		images.forEach((image) => {
			const div = document.createElement("div");
			const img = document.createElement("img");
			const img2 = document.createElement("img");
			img2.addEventListener("click", removeImage(image));
			img2.setAttribute("src", "https://cdn-icons-png.flaticon.com/512/2997/2997911.png");
			img2.classList.add("cross-icon");
			img.setAttribute("src", image.imageUrl);
			img.classList.add("show-image");
			div.classList.add("position-relative");
			div.append(img2);
			div.append(img);
			imagesDiv.append(div);
		});
	});
}

document.getElementById("imageFile").addEventListener("change", (e) => {
	if (e.target.files && e.target.files[0]) {
		currentFile = e.target.files[0];
		const url = URL.createObjectURL(e.target.files[0]);
		const imgDiv = document.getElementById("show-image");
		if (imgDiv) {
			imgDiv.setAttribute("src", url);
			imgDiv.style.display = "block";
		}
		document.getElementById("confirm-btn").style.display = "block";
		document.getElementById("cancel-btn").style.display = "block";
	}
});

document.getElementById("cancel-btn").addEventListener("click", () => {
	document.getElementById("show-image").style.display = "none";
	document.getElementById("confirm-btn").style.display = "none";
	document.getElementById("cancel-btn").style.display = "none";
});

document.getElementById("confirm-btn").addEventListener("click", () => {
	document.getElementById("show-image").style.display = "none";
	document.getElementById("confirm-btn").style.display = "none";
	document.getElementById("cancel-btn").style.display = "none";
	const formData = new FormData();
	formData.append("appId", window.appId);
	formData.append("image", currentFile);
	axios.post(`${url}/v1/admin/carousel`, formData).then((response) => {
		getCarousal(appId);
		buildfire.messaging.sendMessageToWidget({ section: "updateContent" });
	});
});

document.getElementById("save-btn").addEventListener("click", async () => {
	const edit = await editor;
	const html = edit.data.get();
	axios
		.post(`${url}/v1/admin/content`, {
			contentText: html ?? "",
			appId,
		})
		.then(() => {
			buildfire.messaging.sendMessageToWidget({ section: "updateContent" });
		});
});

document.getElementById("sortRooms").addEventListener("change", async (e) => {
	const sort = e.target.value;
	if (sort === "1") {
		rooms = rooms.sort((i, j) => new Date(i.createdAt) - new Date(j.createdAt));
	} else {
		rooms = rooms.sort((i, j) => new Date(j.createdAt) - new Date(i.createdAt));
	}
	previewRooms();
});

function searchRooms(text) {
	visibleRooms = rooms.filter((i) => {
		console.log(i.name.toLowerCase(), text.toLowerCase());
		return i.name.toLowerCase().match(new RegExp(text.toLowerCase()));
	});
	previewRooms();
}

document.getElementById("searchRoom").addEventListener("keydown", async (e) => {
	if (e.key === "Enter") {
		const text = e.target.value;
		searchRooms(text);
	}
});

document.getElementById("searchRoomButton").addEventListener("click", async (e) => {
	const text = document.getElementById("searchRoom").value;
	searchRooms(text);
});
